defmodule Invitator do
  alias Invitator.{
    Customer,
    Distance
  }

  @office_coords {53.339428, -6.257664}
  @distance_for_guests_in_km 100

  def save_to_file(guest_list, filename \\ "output.txt") do
    formatted_guest_list =
      guest_list
      |> Stream.map(&to_string/1)
      |> Enum.join("\n")

    File.write(filename, formatted_guest_list)
  end

  def guest_list(customers) do
    customers
    |> Stream.filter(&within_distance/1)
    |> Enum.sort_by(& &1.user_id)
  end

  defp within_distance(%Customer{} = customer) do
    Distance.in_km_between(
      {customer.lat, customer.long},
      @office_coords
    ) <= @distance_for_guests_in_km
  end
end
