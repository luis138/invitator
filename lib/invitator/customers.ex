defmodule Invitator.Customers do
  @customer_url "https://s3.amazonaws.com/intercom-take-home-test/customers.txt"

  alias Invitator.Customer

  def load!(url \\ @customer_url) do
    case load(url) do
      {:ok, customers} -> customers
      _ -> raise "Could not load customers"
    end
  end

  def load(url \\ @customer_url) do
    case Tesla.get(url) do
      {:ok, %Tesla.Env{status: 200, body: body}} ->
        {:ok, create_customers(body)}

      {:ok, env} ->
        {:error, env}

      _ ->
        raise "Could not load customers"
    end
  end

  defp create_customers(data) do
    data
    |> prepare_data()
    |> Enum.map(&Customer.load/1)
  end

  defp prepare_data(data) do
    data
    |> String.split("\n")
    |> Stream.map(&Jason.decode!/1)
  end
end
