defmodule Invitator.Customer do
  defstruct [:lat, :long, :user_id, :name]

  def load(%{"latitude" => lat, "longitude" => long, "user_id" => user_id, "name" => name}) do
    %__MODULE__{
      lat: to_float(lat),
      long: to_float(long),
      user_id: user_id,
      name: name
    }
  end

  defp to_float(string) when is_bitstring(string) do
    case Float.parse(string) do
      {val, ""} -> val
      _ -> raise "Couldn't parse #{string} to float"
    end
  end

  defimpl String.Chars, for: __MODULE__ do
    def to_string(customer) do
      "User #{customer.user_id}: #{customer.name}"
    end
  end
end
