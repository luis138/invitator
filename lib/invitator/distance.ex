defmodule Invitator.Distance do
  @pi_over_180 :math.pi() / 180.0
  @radius_of_earth_km 6371.0088

  def in_km_between(point, point), do: 0

  def in_km_between({long1, lat1}, {long2, lat2}) do
    converted_lat1 = to_radians(lat1)
    converted_lat2 = to_radians(lat2)
    long_diference = to_radians(long1 - long2)

    central_angle =
      :math.acos(
        :math.sin(converted_lat1) * :math.sin(converted_lat2) +
          :math.cos(converted_lat1) * :math.cos(converted_lat2) * :math.cos(long_diference)
      )

    @radius_of_earth_km * central_angle
  end

  defp to_radians(degrees) do
    degrees * @pi_over_180
  end
end
