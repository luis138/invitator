# Invitator

Takes a list of customers and decides which should be invited to the party.

## Installation

To run Invitator you will need to install Elixir and Erlang. My personal
recommendation is to use https://github.com/asdf-vm/asdf to manage those
installations.

If for some reason you don't want to or can't use asdf, you can follow the
official Elixir installation (https://elixir-lang.org/install.html)

## Running

Before running, ensure you have all the dependencies with `mix deps.get`.

The easiest way of running Invitator is to start `iex` with `iex -S mix` and
then run:

```
> Invitator.Customers.load!() |> Invitator.guest_list()
```

Alternatively you can format and save the results to a file:

```
> Invitator.Customers.load!() |> Invitator.guest_list() |> Invitator.save_to_file()
```

## Testing

Before testing, ensure you have all the dependencies with `mix deps.get`.

Run `mix test` to run the entire test suite.
