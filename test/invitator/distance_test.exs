defmodule Invitator.DistanceTest do
  use ExUnit.Case

  alias Invitator.Distance

  describe "between/2" do
    test "is zero for the same point" do
      point = {50, 60}

      assert Distance.in_km_between(point, point) == 0
    end

    test "calculates distance for different points" do
      point1 = {50, 60}
      point2 = {90, 62}

      assert Distance.in_km_between(point1, point2) == 2133.0710999343096
    end
  end
end
