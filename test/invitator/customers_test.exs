defmodule Invitator.CustomersTest do
  use ExUnit.Case
  use Invitator.VcrCase

  alias Invitator.{
    Customer,
    Customers
  }

  describe "load/1" do
    test "loads customers from the default location" do
      use_cassette "customers" do
        {:ok, customers} = Customers.load()

        assert length(customers) == 32

        assert hd(customers) == %Customer{
                 lat: 52.986375,
                 long: -6.043701,
                 name: "Christina McArdle",
                 user_id: 12
               }
      end
    end
  end
end
