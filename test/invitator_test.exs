defmodule InvitatorTest do
  use ExUnit.Case
  use Invitator.VcrCase

  alias Invitator.{
    Customer,
    Customers
  }

  describe "guest_list/1" do
    test "ignores customers outside of range" do
      guest = %Customer{lat: 53, long: -6, user_id: 1}
      non_guest = %Customer{lat: 63, long: 6, user_id: 2}

      guests = Invitator.guest_list([guest, non_guest])

      assert guests == [guest]
    end

    test "sorts by ascending user id" do
      guest = %Customer{lat: 53, long: -6, user_id: 1}
      non_guest = %Customer{lat: 53, long: -6.2, user_id: 2}

      guests = Invitator.guest_list([non_guest, guest])

      assert guests == [guest, non_guest]
    end

    test "finds all the guests with a real example" do
      use_cassette "customers" do
        {:ok, customers} = Customers.load()

        guests = Invitator.guest_list(customers)

        assert length(guests) == 12
      end
    end
  end
end
