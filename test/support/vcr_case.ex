defmodule Invitator.VcrCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

      setup do
        ExVCR.Config.cassette_library_dir("test/support/vcr_cassettes")

        :ok
      end
    end
  end
end
